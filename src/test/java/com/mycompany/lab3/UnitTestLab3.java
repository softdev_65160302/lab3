package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class UnitTestLab3 {
    
    public UnitTestLab3() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Vertical1_output_true(){
        String[][] table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Vertical2_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Vertical3_output_true(){
        String[][] table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Vertical1_output_true(){
        String[][] table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Vertical2_output_true(){
        String[][] table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Vertical3_output_true(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Horizontal1_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Horizontal2_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Horizontal3_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Horizontal1_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Horizontal2_output_true(){
        String[][] table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Horizontal3_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Diagonal1_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Diagonal1_output_true(){
        String[][] table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Diagonal2_output_true(){
        String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Diagonal2_output_true(){
        String[][] table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table, currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckDraw_output_true(){
        String[][] table = {{"X","O","X"},{"X","O","O"},{"O","X","O"}};
        boolean result = Lab3.checkDraw(table);
        assertEquals(true, result);
    }
}
